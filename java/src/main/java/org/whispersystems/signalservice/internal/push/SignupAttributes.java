package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Ayham Hassan on 6/24/2016.
 */
public class SignupAttributes {
    @JsonProperty
    private String username;

    @JsonProperty
    private String number;

    @JsonProperty
    private String transport;

    public SignupAttributes(String username, String number, String transport) {
        this.username = username;
        this.number = number;
        this.transport = transport;
    }

    public String getTransport() {
        return transport;
    }

    public String getUsername() {
        return username;
    }

    public String getNumber() {
        return number;
    }

}
