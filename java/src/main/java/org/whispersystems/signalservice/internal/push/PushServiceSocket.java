/**
 * Copyright (C) 2014 Open Whisper Systems
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.http.conn.ssl.StrictHostnameVerifier;
import org.apache.http.util.TextUtils;
import org.whispersystems.libsignal.IdentityKey;
import org.whispersystems.libsignal.ecc.ECPublicKey;
import org.whispersystems.libsignal.logging.Log;
import org.whispersystems.libsignal.state.PreKeyBundle;
import org.whispersystems.libsignal.state.PreKeyRecord;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;
import org.whispersystems.libsignal.util.guava.Optional;
import org.whispersystems.signalservice.api.crypto.AttachmentCipherOutputStream;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment.ProgressListener;
import org.whispersystems.signalservice.api.messages.multidevice.DeviceInfo;
import org.whispersystems.signalservice.api.push.ContactTokenDetails;
import org.whispersystems.signalservice.api.push.SignedPreKeyEntity;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.push.TrustStore;
import org.whispersystems.signalservice.api.push.exceptions.AuthorizationFailedException;
import org.whispersystems.signalservice.api.push.exceptions.ExpectationFailedException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.api.push.exceptions.NotFoundException;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;
import org.whispersystems.signalservice.api.push.exceptions.RateLimitException;
import org.whispersystems.signalservice.api.push.exceptions.UnregisteredUserException;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.internal.push.exceptions.InvalidTransporterVerifier;
import org.whispersystems.signalservice.internal.push.exceptions.MismatchedDevicesException;
import org.whispersystems.signalservice.internal.push.exceptions.StaleDevicesException;
import org.whispersystems.signalservice.internal.util.Base64;
import org.whispersystems.signalservice.internal.util.BlacklistingTrustManager;
import org.whispersystems.signalservice.internal.util.JsonUtil;
import org.whispersystems.signalservice.internal.util.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

/**
 * @author Moxie Marlinspike
 */
public class PushServiceSocket {

	public static final  String ACCOUNT_VERIFICATION_TRANSPORT_SMS   = "sms";
	public static final  String ACCOUNT_VERIFICATION_TRANSPORT_EMAIL = "email";
	public static final  String ACCOUNT_VERIFICATION_TRANSPORT_VOICE = "voice";
	private static final String TAG                                  = PushServiceSocket.class
			                                                                   .getSimpleName ( );
	//  private static final String CREATE_ACCOUNT_SMS_PATH   = "/v1/accounts/sms/code/%s";
//  private static final String CREATE_ACCOUNT_VOICE_PATH = "/v1/accounts/voice/code/%s";
	private static final String CREATE_ACCOUNT_SIGNUP                =
			"/v1/accounts/signup/?client=android";
	private static final String VERIFY_ACCOUNT_CODE_PATH             = "/v1/accounts/code/%s";
	private static final String VERIFY_ACCOUNT_TOKEN_PATH            = "/v1/accounts/token/%s";
	private static final String REGISTER_GCM_PATH                    = "/v1/accounts/gcm/";
	private static final String REGISTER_EPN_PATH                    = "/v1/accounts/epn/";
	private static final String REQUEST_TOKEN_PATH                   = "/v1/accounts/token";
	private static final String SET_ACCOUNT_ATTRIBUTES               = "/v1/accounts/attributes/";

	private static final String PREKEY_METADATA_PATH = "/v2/keys/";
	private static final String PREKEY_PATH          = "/v2/keys/%s";
	private static final String PREKEY_DEVICE_PATH   = "/v2/keys/%s/%s";
	private static final String SIGNED_PREKEY_PATH   = "/v2/keys/signed";

	private static final String PROVISIONING_CODE_PATH    = "/v1/devices/provisioning/code";
	private static final String PROVISIONING_MESSAGE_PATH = "/v1/provisioning/%s";
	private static final String DEVICE_PATH               = "/v1/devices/%s";

	private static final String DIRECTORY_TOKENS_PATH    = "/v1/directory/tokens";
	private static final String DIRECTORY_VERIFY_PATH    = "/v1/directory/%s";
	private static final String MESSAGE_PATH             = "/v1/messages/%s";
	private static final String ACKNOWLEDGE_MESSAGE_PATH = "/v1/messages/%s/%d";
	private static final String RECEIPT_PATH             = "/v1/receipt/%s/%d";
	private static final String CONFIRM_ACK_PATH         = "/v1/confirm/ack/%s";
	private static final String CONFIRM_ASK_PATH         = "/v1/confirm/ask/%s";
	private static final String ATTACHMENT_PATH          = "/v1/attachments/%s";

	private final String              serviceUrl;
	private final TrustManager[]      trustManagers;
	private final CredentialsProvider credentialsProvider;
	private final String              userAgent;

	public PushServiceSocket ( String serviceUrl, TrustStore trustStore, CredentialsProvider
			                                                                     credentialsProvider, String userAgent ) {
		this.serviceUrl = serviceUrl;
		this.credentialsProvider = credentialsProvider;
		this.trustManagers = BlacklistingTrustManager.createFor ( trustStore );
		this.userAgent = userAgent;
	}

	/**
	 * requrest a new account
	 *
	 * @param verificationTransporter specify how to transport verification code EMAIL|SMS|VOICE.
	 *                                in case of email , username will be used otherwise
	 *                                phoneNumber will be used
	 * @param phoneNumber             phone number that will be used to send the verification code
	 *                                   to it in
	 *                                case of specify SMS | VOICE as a verification transport
	 * @throws IOException
	 */
	public void createAccount ( String verificationTransporter, String phoneNumber ) throws
			IOException {
		if ( ! ( verificationTransporter.equals ( ACCOUNT_VERIFICATION_TRANSPORT_EMAIL ) ||
				         verificationTransporter.equals ( ACCOUNT_VERIFICATION_TRANSPORT_SMS ) ||
				         verificationTransporter.equals ( ACCOUNT_VERIFICATION_TRANSPORT_VOICE )
		) )
			throw new InvalidTransporterVerifier ( );

		if ( ( verificationTransporter.equals ( ACCOUNT_VERIFICATION_TRANSPORT_VOICE ) ||
				       verificationTransporter.equals ( ACCOUNT_VERIFICATION_TRANSPORT_SMS ) ) &&
				     TextUtils.isEmpty ( phoneNumber ) )
			throw new InvalidTransporterVerifier ( );

		SignupAttributes signupEntity = new SignupAttributes ( credentialsProvider.getUser ( ),
				                                                     phoneNumber,
				                                                     verificationTransporter );
		makeRequest ( CREATE_ACCOUNT_SIGNUP, "PUT", JsonUtil.toJson ( signupEntity ) );
	}

	private String makeRequest ( String urlFragment, String method, String body )
			throws NonSuccessfulResponseCodeException, PushNetworkException {
		Response response = getConnection ( urlFragment, method, body );

		int    responseCode;
		String responseMessage;
		String responseBody;

		try {
			responseCode = response.code ( );
			responseMessage = response.message ( );
			responseBody = response.body ( ).string ( );
		}
		catch ( IOException ioe ) {
			throw new PushNetworkException ( ioe );
		}

		switch ( responseCode ) {
			case 413:
				throw new RateLimitException ( "Rate limit exceeded: " + responseCode );
			case 401:
			case 403:
				throw new AuthorizationFailedException ( "Authorization failed!" );
			case 404:
				throw new NotFoundException ( "Not found" );
			case 409:
				MismatchedDevices mismatchedDevices;

				try {
					mismatchedDevices = JsonUtil.fromJson ( responseBody, MismatchedDevices
							                                                      .class );
				}
				catch ( JsonProcessingException e ) {
					Log.w ( TAG, e );
					throw new NonSuccessfulResponseCodeException ( "Bad response: " + responseCode
							                                               + " " +
							                                               responseMessage );
				}
				catch ( IOException e ) {
					throw new PushNetworkException ( e );
				}

				throw new MismatchedDevicesException ( mismatchedDevices );
			case 410:
				StaleDevices staleDevices;

				try {
					staleDevices = JsonUtil.fromJson ( responseBody, StaleDevices.class );
				}
				catch ( JsonProcessingException e ) {
					throw new NonSuccessfulResponseCodeException ( "Bad response: " + responseCode
							                                               + " " +
							                                               responseMessage );
				}
				catch ( IOException e ) {
					throw new PushNetworkException ( e );
				}

				throw new StaleDevicesException ( staleDevices );
			case 411:
				DeviceLimit deviceLimit;

				try {
					deviceLimit = JsonUtil.fromJson ( responseBody, DeviceLimit.class );
				}
				catch ( JsonProcessingException e ) {
					throw new NonSuccessfulResponseCodeException ( "Bad response: " + responseCode
							                                               + " " +
							                                               responseMessage );
				}
				catch ( IOException e ) {
					throw new PushNetworkException ( e );
				}

				throw new DeviceLimitExceededException ( deviceLimit );
			case 417:
				throw new ExpectationFailedException ( );
		}

		if ( responseCode != 200 && responseCode != 204 ) {
			throw new NonSuccessfulResponseCodeException ( "Bad response: " + responseCode + " " +
					                                               responseMessage );
		}

		return responseBody;
	}

	private Response getConnection ( String urlFragment, String method, String body )
			throws PushNetworkException {
		try {
			Log.w ( TAG, "Push service URL: " + serviceUrl );
			Log.w ( TAG, "Opening URL: " + String.format ( "%s%s", serviceUrl, urlFragment ) );


			SSLContext context = SSLContext.getInstance ( "TLS" );
			context.init ( null, trustManagers, null );

			OkHttpClient okHttpClient = new OkHttpClient ( );
			okHttpClient.setSslSocketFactory ( context.getSocketFactory ( ) );

			okHttpClient.setHostnameVerifier ( new StrictHostnameVerifier ( ) );

			Request.Builder request = new Request.Builder ( );
			request.url ( String.format ( "%s%s", serviceUrl, urlFragment ) );

			if ( body != null ) {
				request.method ( method, RequestBody.create ( MediaType.parse ( "application/json"
				), body ) );
			} else {
				request.method ( method, null );
			}

			if ( credentialsProvider.getPassword ( ) != null ) {
				request.addHeader ( "Authorization", getAuthorizationHeader ( ) );
			}

			if ( userAgent != null ) {
				request.addHeader ( "X-Signal-Agent", userAgent );
			}

			return okHttpClient.newCall ( request.build ( ) ).execute ( );
		}
		catch ( IOException e ) {
			throw new PushNetworkException ( e );
		}
		catch ( NoSuchAlgorithmException | KeyManagementException e ) {
			throw new AssertionError ( e );
		}
	}

	private String getAuthorizationHeader ( ) {
		try {
			return "Basic " + Base64.encodeBytes ( ( credentialsProvider.getUser ( ) + ":" +
					                                         credentialsProvider.getPassword ( )
			).getBytes ( "UTF-8" ) );
		}
		catch ( UnsupportedEncodingException e ) {
			throw new AssertionError ( e );
		}
	}

	public void verifyAccountCode (
			                              String verificationCode,
			                              String signalingKey, int
					                                                   registrationId,
			                              boolean voice,
			                              boolean supportEtkenPushNotification,
			                              String epnUsername,
			                              String epnPassword)
			throws IOException {
		AccountAttributes signalingKeyEntity = new AccountAttributes ( signalingKey,
				                                                             registrationId,
				                                                             voice,
				                                                             supportEtkenPushNotification,
				                                                             epnUsername,
				                                                             epnPassword);
		makeRequest ( String.format ( VERIFY_ACCOUNT_CODE_PATH, verificationCode ),
				"PUT", JsonUtil.toJson ( signalingKeyEntity ) );
	}

	public void verifyAccountToken ( String verificationToken, String signalingKey,
	                                 int   registrationId,
	                                 boolean voice,
	                                 boolean supportEtkenPushNotification,
	                                 String epnUsername,
	                                 String epnPassword)
			throws IOException {
		AccountAttributes signalingKeyEntity = new AccountAttributes (
				                                                             signalingKey,
				                                                             registrationId,
				                                                             voice ,
				                                                             supportEtkenPushNotification,
				                                                             epnUsername,
				                                                             epnPassword);
		makeRequest ( String.format ( VERIFY_ACCOUNT_TOKEN_PATH, verificationToken ),
				"PUT", JsonUtil.toJson ( signalingKeyEntity ) );
	}

	public void setAccountAttributes ( String signalingKey,
	                                   int registrationId,
	                                   boolean voice)
			throws IOException {
		AccountAttributes accountAttributes = new AccountAttributes ( signalingKey,
				                                                            registrationId,
				                                                            voice,
				                                                            false,
				                                                            null,
				                                                            null);
		makeRequest ( SET_ACCOUNT_ATTRIBUTES, "PUT", JsonUtil.toJson ( accountAttributes ) );
	}

	public String getAccountVerificationToken ( ) throws IOException {
		String responseText = makeRequest ( REQUEST_TOKEN_PATH, "GET", null );
		return JsonUtil.fromJson ( responseText, AuthorizationToken.class ).getToken ( );
	}

	public String getNewDeviceVerificationCode ( ) throws IOException {
		String responseText = makeRequest ( PROVISIONING_CODE_PATH, "GET", null );
		return JsonUtil.fromJson ( responseText, DeviceCode.class ).getVerificationCode ( );
	}

	public List< DeviceInfo > getDevices ( ) throws IOException {
		String responseText = makeRequest ( String.format ( DEVICE_PATH, "" ), "GET", null );
		return JsonUtil.fromJson ( responseText, DeviceInfoList.class ).getDevices ( );
	}

	public void removeDevice ( long deviceId ) throws IOException {
		makeRequest ( String.format ( DEVICE_PATH, String.valueOf ( deviceId ) ), "DELETE", null );
	}

	public void sendProvisioningMessage ( String destination, byte[] body ) throws IOException {
		makeRequest ( String.format ( PROVISIONING_MESSAGE_PATH, destination ), "PUT",
				JsonUtil.toJson ( new ProvisioningMessage ( Base64.encodeBytes ( body ) ) ) );
	}

	public void sendReceipt ( String destination, long messageId, Optional< String > relay )
			throws IOException {
		String path = String.format ( RECEIPT_PATH, destination, messageId );

		if ( relay.isPresent ( ) ) {
			path += "?relay=" + relay.get ( );
		}

		makeRequest ( path, "PUT", null );
	}

	public void sendConfirmAck ( String destination, Optional< String > relay )
			throws IOException {
		String path = String.format ( CONFIRM_ACK_PATH, destination );

		if ( relay.isPresent ( ) ) {
			path += "?relay=" + relay.get ( );
		}

		makeRequest ( path, "PUT", null );
	}

	public void sendAskConfirm ( String destination, Optional< String > relay )
			throws IOException {
		String path = String.format ( CONFIRM_ASK_PATH, destination );

		if ( relay.isPresent ( ) ) {
			path += "?relay=" + relay.get ( );
		}

		makeRequest ( path, "PUT", null );
	}

	public void registerGcmId ( String gcmRegistrationId ) throws IOException {
		GcmRegistrationId registration = new GcmRegistrationId ( gcmRegistrationId, true );
		makeRequest ( REGISTER_GCM_PATH, "PUT", JsonUtil.toJson ( registration ) );
	}

	public void registerEpn ( String epnUsername, String epnPassword ) throws IOException {
		EpnRegistrationEntity registration = new EpnRegistrationEntity ( epnUsername, epnPassword );
		makeRequest ( REGISTER_EPN_PATH, "PUT", JsonUtil.toJson ( registration ) );
	}

	public void unregisterGcmId ( ) throws IOException {
		makeRequest ( REGISTER_GCM_PATH, "DELETE", null );
	}

	public SendMessageResponse sendMessage ( OutgoingPushMessageList bundle )
			throws IOException {
		try {
			String responseText = makeRequest ( String.format ( MESSAGE_PATH, bundle
					                                                                  .getDestination ( ) ), "PUT", JsonUtil.toJson ( bundle ) );

			if ( responseText == null ) return new SendMessageResponse ( false );
			else return JsonUtil.fromJson ( responseText, SendMessageResponse.class );
		}
		catch ( NotFoundException nfe ) {
			throw new UnregisteredUserException ( bundle.getDestination ( ), nfe );
		}
	}

	public List< SignalServiceEnvelopeEntity > getMessages ( ) throws IOException {
		String responseText = makeRequest ( String.format ( MESSAGE_PATH, "" ), "GET", null );
		return JsonUtil.fromJson ( responseText, SignalServiceEnvelopeEntityList.class )
				       .getMessages ( );
	}

	public void acknowledgeMessage ( String sender, long timestamp ) throws IOException {
		makeRequest ( String.format ( ACKNOWLEDGE_MESSAGE_PATH, sender, timestamp ), "DELETE",
				null );
	}

	public void registerPreKeys ( IdentityKey identityKey,
	                              PreKeyRecord lastResortKey,
	                              SignedPreKeyRecord signedPreKey,
	                              List< PreKeyRecord > records )
			throws IOException {
		List< PreKeyEntity > entities = new LinkedList<> ( );

		for ( PreKeyRecord record : records ) {
			PreKeyEntity entity = new PreKeyEntity ( record.getId ( ),
					                                       record.getKeyPair ( ).getPublicKey ( ) );

			entities.add ( entity );
		}

		PreKeyEntity lastResortEntity = new PreKeyEntity ( lastResortKey.getId ( ),
				                                                 lastResortKey.getKeyPair ( )
						                                                 .getPublicKey ( ) );

		SignedPreKeyEntity signedPreKeyEntity = new SignedPreKeyEntity ( signedPreKey.getId ( ),
				                                                               signedPreKey
						                                                               .getKeyPair
								                                                                ( ).getPublicKey ( ),

				                                                               signedPreKey
						                                                               .getSignature ( ) );

		makeRequest ( String.format ( PREKEY_PATH, "" ), "PUT",
				JsonUtil.toJson ( new PreKeyState ( entities, lastResortEntity,
						                                  signedPreKeyEntity, identityKey ) ) );
	}

	public int getAvailablePreKeys ( ) throws IOException {
		String       responseText = makeRequest ( PREKEY_METADATA_PATH, "GET", null );
		PreKeyStatus preKeyStatus = JsonUtil.fromJson ( responseText, PreKeyStatus.class );

		return preKeyStatus.getCount ( );
	}

	public List< PreKeyBundle > getPreKeys ( SignalServiceAddress destination, int deviceIdInteger
	) throws IOException {
		try {
			String deviceId = String.valueOf ( deviceIdInteger );

			if ( deviceId.equals ( "1" ) )
				deviceId = "*";

			String path = String.format ( PREKEY_DEVICE_PATH, destination.getUsername ( ),
					deviceId );

			if ( destination.getRelay ( ).isPresent ( ) ) {
				path = path + "?relay=" + destination.getRelay ( ).get ( );
			}

			String responseText = makeRequest ( path, "GET", null );
			PreKeyResponse response = JsonUtil.fromJson ( responseText, PreKeyResponse
					                                                            .class );
			List< PreKeyBundle > bundles = new LinkedList<> ( );

			for ( PreKeyResponseItem device : response.getDevices ( ) ) {
				ECPublicKey preKey                = null;
				ECPublicKey signedPreKey          = null;
				byte[]      signedPreKeySignature = null;
				int         preKeyId              = - 1;
				int         signedPreKeyId        = - 1;

				if ( device.getSignedPreKey ( ) != null ) {
					signedPreKey = device.getSignedPreKey ( ).getPublicKey ( );
					signedPreKeyId = device.getSignedPreKey ( ).getKeyId ( );
					signedPreKeySignature = device.getSignedPreKey ( ).getSignature ( );
				}

				if ( device.getPreKey ( ) != null ) {
					preKeyId = device.getPreKey ( ).getKeyId ( );
					preKey = device.getPreKey ( ).getPublicKey ( );
				}

				bundles.add ( new PreKeyBundle ( device.getRegistrationId ( ), device.getDeviceId
						                                                                      ( ),
						                               preKeyId,
						                               preKey, signedPreKeyId, signedPreKey,
						                               signedPreKeySignature,
						                               response.getIdentityKey ( ) ) );
			}

			return bundles;
		}
		catch ( NotFoundException nfe ) {
			throw new UnregisteredUserException ( destination.getUsername ( ), nfe );
		}
	}

	public PreKeyBundle getPreKey ( SignalServiceAddress destination, int deviceId ) throws
			IOException {
		try {
			String path = String.format ( PREKEY_DEVICE_PATH, destination.getUsername ( ),
					String.valueOf ( deviceId ) );

			if ( destination.getRelay ( ).isPresent ( ) ) {
				path = path + "?relay=" + destination.getRelay ( ).get ( );
			}

			String         responseText = makeRequest ( path, "GET", null );
			PreKeyResponse response     = JsonUtil.fromJson ( responseText, PreKeyResponse.class );

			if ( response.getDevices ( ) == null || response.getDevices ( ).size ( ) < 1 )
				throw new IOException ( "Empty prekey list" );

			PreKeyResponseItem device                = response.getDevices ( ).get ( 0 );
			ECPublicKey        preKey                = null;
			ECPublicKey        signedPreKey          = null;
			byte[]             signedPreKeySignature = null;
			int                preKeyId              = - 1;
			int                signedPreKeyId        = - 1;

			if ( device.getPreKey ( ) != null ) {
				preKeyId = device.getPreKey ( ).getKeyId ( );
				preKey = device.getPreKey ( ).getPublicKey ( );
			}

			if ( device.getSignedPreKey ( ) != null ) {
				signedPreKeyId = device.getSignedPreKey ( ).getKeyId ( );
				signedPreKey = device.getSignedPreKey ( ).getPublicKey ( );
				signedPreKeySignature = device.getSignedPreKey ( ).getSignature ( );
			}

			return new PreKeyBundle ( device.getRegistrationId ( ), device.getDeviceId ( ),
					                        preKeyId, preKey,
					                        signedPreKeyId, signedPreKey, signedPreKeySignature,
					                        response.getIdentityKey ( ) );
		}
		catch ( NotFoundException nfe ) {
			throw new UnregisteredUserException ( destination.getUsername ( ), nfe );
		}
	}

	public SignedPreKeyEntity getCurrentSignedPreKey ( ) throws IOException {
		try {
			String responseText = makeRequest ( SIGNED_PREKEY_PATH, "GET", null );
			return JsonUtil.fromJson ( responseText, SignedPreKeyEntity.class );
		}
		catch ( NotFoundException e ) {
			Log.w ( TAG, e );
			return null;
		}
	}

	public void setCurrentSignedPreKey ( SignedPreKeyRecord signedPreKey ) throws IOException {
		SignedPreKeyEntity signedPreKeyEntity = new SignedPreKeyEntity ( signedPreKey.getId ( ),
				                                                               signedPreKey
						                                                               .getKeyPair
								                                                                ( ).getPublicKey ( ),
				                                                               signedPreKey
						                                                               .getSignature ( ) );
		makeRequest ( SIGNED_PREKEY_PATH, "PUT", JsonUtil.toJson ( signedPreKeyEntity ) );
	}

	public long sendAttachment ( PushAttachmentData attachment ) throws IOException {
		String response = makeRequest ( String.format ( ATTACHMENT_PATH, "" ),
				"GET", null );
		AttachmentDescriptor attachmentKey = JsonUtil.fromJson ( response, AttachmentDescriptor
				                                                                   .class );

		if ( attachmentKey == null || attachmentKey.getLocation ( ) == null ) {
			throw new IOException ( "Server failed to allocate an attachment key!" );
		}

		Log.w ( TAG, "Got attachment content location: " + attachmentKey.getLocation ( ) );

		uploadAttachment ( "PUT", attachmentKey.getLocation ( ), attachment.getData ( ),
				attachment.getDataSize ( ), attachment.getKey ( ), attachment.getListener ( ) );

		return attachmentKey.getId ( );
	}

	private void uploadAttachment ( String method, String url, InputStream data,
	                                long dataSize, byte[] key, ProgressListener listener )
			throws IOException {
		URL                uploadUrl  = new URL ( url );
		HttpsURLConnection connection = ( HttpsURLConnection ) uploadUrl.openConnection ( );
		connection.setDoOutput ( true );

		if ( dataSize > 0 ) {
			connection.setFixedLengthStreamingMode ( ( int ) AttachmentCipherOutputStream
					                                                 .getCiphertextLength (
							                                                 dataSize ) );
		} else {
			connection.setChunkedStreamingMode ( 0 );
		}

		connection.setRequestMethod ( method );
		connection.setRequestProperty ( "Content-Type", "application/octet-stream" );
		connection.setRequestProperty ( "Connection", "close" );
		connection.connect ( );

		try {
			OutputStream stream = connection.getOutputStream ( );
			AttachmentCipherOutputStream out = new AttachmentCipherOutputStream ( key,
					                                                                    stream );
			byte[] buffer        = new byte[ 4096 ];
			int    read, written = 0;

			while ( ( read = data.read ( buffer ) ) != - 1 ) {
				out.write ( buffer, 0, read );
				written += read;

				if ( listener != null ) {
					listener.onAttachmentProgress ( dataSize, written );
				}
			}

			data.close ( );
			out.flush ( );
			out.close ( );

			if ( connection.getResponseCode ( ) != 200 ) {
				throw new IOException ( "Bad response: " + connection.getResponseCode ( ) + " " +
						                        connection.getResponseMessage ( ) );
			}
		}
		finally {
			connection.disconnect ( );
		}
	}

	public void retrieveAttachment ( String relay, long attachmentId, File destination,
	                                 ProgressListener listener ) throws IOException {
		String path = String.format ( ATTACHMENT_PATH, String.valueOf ( attachmentId ) );

		if ( ! Util.isEmpty ( relay ) ) {
			path = path + "?relay=" + relay;
		}

		String response = makeRequest ( path, "GET", null );
		AttachmentDescriptor descriptor = JsonUtil.fromJson ( response, AttachmentDescriptor
				                                                                .class );

		Log.w ( TAG, "Attachment: " + attachmentId + " is at: " + descriptor.getLocation ( ) );

		downloadExternalFile ( descriptor.getLocation ( ), destination, listener );
	}

	private void downloadExternalFile ( String url, File localDestination, ProgressListener
			                                                                       listener )
			throws IOException {
		URL               downloadUrl = new URL ( url );
		HttpURLConnection connection  = ( HttpURLConnection ) downloadUrl.openConnection ( );
		connection.setRequestProperty ( "Content-Type", "application/octet-stream" );
		connection.setRequestMethod ( "GET" );
		connection.setDoInput ( true );

		try {
			if ( connection.getResponseCode ( ) != 200 ) {
				throw new NonSuccessfulResponseCodeException ( "Bad response: " + connection
						                                                                  .getResponseCode ( ) );
			}

			OutputStream output          = new FileOutputStream ( localDestination );
			InputStream  input           = connection.getInputStream ( );
			byte[]       buffer          = new byte[ 4096 ];
			int          contentLength   = connection.getContentLength ( );
			int          read, totalRead = 0;

			while ( ( read = input.read ( buffer ) ) != - 1 ) {
				output.write ( buffer, 0, read );
				totalRead += read;

				if ( listener != null ) {
					listener.onAttachmentProgress ( contentLength, totalRead );
				}
			}

			output.close ( );
			Log.w ( TAG, "Downloaded: " + url + " to: " + localDestination.getAbsolutePath ( ) );
		}
		catch ( IOException ioe ) {
			throw new PushNetworkException ( ioe );
		}
		finally {
			connection.disconnect ( );
		}
	}

	public List< ContactTokenDetails > retrieveDirectory ( Set< String > contactTokens )
			throws NonSuccessfulResponseCodeException, PushNetworkException {
		try {
			ContactTokenList contactTokenList = new ContactTokenList ( new LinkedList<> ( contactTokens ) );
			String response = makeRequest ( DIRECTORY_TOKENS_PATH, "PUT",
					JsonUtil.toJson ( contactTokenList ) );
			ContactTokenDetailsList activeTokens = JsonUtil.fromJson ( response,
					ContactTokenDetailsList.class );

			return activeTokens.getContacts ( );
		}
		catch ( IOException e ) {
			Log.w ( TAG, e );
			throw new NonSuccessfulResponseCodeException ( "Unable to parse entity" );
		}
	}

	public ContactTokenDetails getContactTokenDetails ( String contactToken ) throws IOException {
		try {
			String response = makeRequest ( String.format ( DIRECTORY_VERIFY_PATH, contactToken ),
					"GET", null );
			return JsonUtil.fromJson ( response, ContactTokenDetails.class );
		}
		catch ( NotFoundException nfe ) {
			return null;
		}
	}

	private static class GcmRegistrationId {

		@JsonProperty
		private String gcmRegistrationId;

		@JsonProperty
		private boolean webSocketChannel;

		public GcmRegistrationId ( ) {}

		public GcmRegistrationId ( String gcmRegistrationId, boolean webSocketChannel ) {
			this.gcmRegistrationId = gcmRegistrationId;
			this.webSocketChannel = webSocketChannel;
		}
	}

	private class EpnRegistrationEntity {

		@JsonProperty
		private String epnUsername;

		@JsonProperty
		private String epnPassword;

		public EpnRegistrationEntity ( String epnUsername, String epnPassword ) {
			this.epnUsername = epnUsername;
			this.epnPassword = epnPassword;
		}

		public String getEpnUsername() {
			return epnUsername;
		}

		public String getEpnPassword() {
			return epnPassword;
		}
	}

	private static class AttachmentDescriptor {
		@JsonProperty
		private long id;

		@JsonProperty
		private String location;

		public long getId ( ) {
			return id;
		}

		public String getLocation ( ) {
			return location;
		}
	}
}
