/**
 * Copyright (C) 2014 Open Whisper Systems
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.whispersystems.signalservice.internal.push;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountAttributes {

	@JsonProperty
	private String signalingKey;

	@JsonProperty
	private int registrationId;

	@JsonProperty
	private boolean voice;

	@JsonProperty
	private boolean supportEPN;

	@JsonProperty
	private String epnUsername;

	@JsonProperty
	private String epnPassword;


	public AccountAttributes ( String signalingKey, int registrationId, boolean voice, boolean supportEPN, String epnUsername, String epnPassword) {
		this.signalingKey = signalingKey;
		this.registrationId = registrationId;
		this.voice = voice;
		this.supportEPN = supportEPN;
		this.epnUsername    = epnUsername;
		this.epnPassword    = epnPassword;
	}

	public AccountAttributes ( ) {}

	public String getSignalingKey ( ) {
		return signalingKey;
	}

	public int getRegistrationId ( ) {
		return registrationId;
	}

	public boolean isVoice ( ) {
		return voice;
	}

	public boolean isSupportEPN ( ) {
		return supportEPN;
	}

	public String getEpnUsername() {
		return epnUsername;
	}

	public String getEpnPassword() {
		return epnPassword;
	}
}
