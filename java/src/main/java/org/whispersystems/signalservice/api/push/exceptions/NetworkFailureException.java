package org.whispersystems.signalservice.api.push.exceptions;

public class NetworkFailureException extends Exception {

  private final String username;

  public NetworkFailureException(String username, Exception nested) {
    super(nested);
    this.username = username;
  }

  public String getUsername() {
    return username;
  }
}
